/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.controller;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Conta;
import br.ufrn.imd.residenciati.exemplopoo.repositories.ContaRepository;

/**
 *
 * @author flawtista
 */
public class ContaController {
    private ContaRepository contaRepository;
    
    public ContaController(ContaRepository contaRepository){
        this.contaRepository = contaRepository;
    }
    
    public void salvar(Conta conta){
        this.contaRepository.addConta(conta);
    }
    
    public void remove(Conta conta){
        this.contaRepository.removeConta(conta);
    }
    
    public void atualizar(Conta conta){
        this.remove(conta);
        this.salvar(conta);
    }
    public List<String> listar(){
         List<String> lista = new ArrayList<String>();
        for (Conta conta : contaRepository.listar()) {
            lista.add(conta.toString());
        }
        return lista;
    } 
}
