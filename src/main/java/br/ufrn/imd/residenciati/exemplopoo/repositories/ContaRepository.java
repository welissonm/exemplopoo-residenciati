/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Conta;
import br.ufrn.imd.residenciati.exemplopoo.repositories.framework.AbstractEntityDao;

/**
 *
 * @author flawtista
 */
public class ContaRepository{
     private final List<Conta> contas;
    
    public ContaRepository(){
        this.contas = new ArrayList<Conta>();
    }
    public void addConta(Conta conta){
        this.contas.add(conta);
    }
    public void removeConta(Conta conta){
        this.contas.remove(conta);
    }
    public List<Conta> listar(){
        return contas;
    }
}
