/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories.framework;

import java.sql.Connection;

/**
 *
 * @author Welisson M Santos
 */
public class SessionBD {
    private ConnectionFactory conectionFactory;
    private SessionBD() {
        this.conectionFactory = new ConnectionFactory(SGBD.MySQL, "localhost/world", 3306, "root", "root");
    }
    
    public Connection getConexao()
    {
        Connection con = null;
        if(!this.conectionFactory.isConnected())
        {
           this.conectionFactory.conectar();
        }
        con = this.conectionFactory.getConexao();
        return con;
    }
    
    public static SessionBD getInstance() {
        return SessionBDHolder.INSTANCE;
    }
    
    private static class SessionBDHolder {

        private static final SessionBD INSTANCE = new SessionBD();
    }
}
