/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Mesa;

/**
 *
 * @author flawtista
 */
public class MesaRepository {
    private final List<Mesa> mesas;
    
    public MesaRepository(){
        this.mesas = new ArrayList<Mesa>();
    }
    public void addMesa(Mesa mesa){
        this.mesas.add(mesa);
    }
    public void removeMesa(Mesa mesa){
        this.mesas.remove(mesa);
    }
    public List<Mesa> listar(){
        return mesas;
    }
}
