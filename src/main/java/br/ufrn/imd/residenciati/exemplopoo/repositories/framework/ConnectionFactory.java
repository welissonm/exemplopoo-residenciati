package br.ufrn.imd.residenciati.exemplopoo.repositories.framework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.ufrn.imd.residenciati.exemplopoo.model.Item;
import br.ufrn.imd.residenciati.exemplopoo.repositories.EntityDao;





public class ConnectionFactory
{
	private String sgbd;
	private DriverJDBC driverJdbc;
	private String url;
	private String user;
	private String password;
	private String database;
	private Connection conexao;
	private int porta;
	public ConnectionFactory()
	{
		this(SGBD.MySQL, "", "localhost", 3306, "root","");
	}
	
	public ConnectionFactory(String url,String user, String password)
	{
		this(SGBD.MySQL, "", url, 3306, user,password);
	}

	public ConnectionFactory(SGBD sgbd, String url,String user, String password)
	{
		this(sgbd, "", url, 3306, user,password);
	}
	
	public ConnectionFactory(SGBD sgbd, String url, int porta, String user, String password)
	{
		this(sgbd, "", url, porta, user,password);
	}
	public ConnectionFactory
	(SGBD sgbd, DriverJDBC driverJdbc, String url,int porta, String user, String password) {
		this(sgbd, driverJdbc, url,"", porta, user, password);
	}
	public ConnectionFactory
	(SGBD sgbd, String url, String database,int porta, String user, String password) {
		this(sgbd, null, url,database, porta, user, password);
	}
	public ConnectionFactory
	(SGBD sgbd, DriverJDBC driverJdbc, String url, String database, int porta, String user, String password)
	{
		this.url = url;
		this.database = database;
		this.user = user;
		this.password = password;
		this.porta = porta;
		this.driverJdbc = driverJdbc;
		switch(sgbd)
		{
			case PostgreSQL:
				this.sgbd = "postgresql";
				this.url = "jdbc:"+this.sgbd+"://"+this.url+":"+String.valueOf(this.porta)+"/"+this.database;
				if(this.driverJdbc == null) {
					this.driverJdbc = DriverJDBC.PostgreSql;
				}
				break;
			case MySQL:
				this.sgbd = "mysql";
				this.url = "jdbc:"+this.sgbd+"://";
				if(this.driverJdbc == null) {
					this.driverJdbc = DriverJDBC.MySql;
				}
				break;
			case Oracle:
				this.sgbd = "oracle";
				this.url = "jdbc:"+this.sgbd+":thin:@";
				break;
			case SQLServer:
				this.sgbd = "sqlserver";
				this.url = "jdbc:microsoft:"+this.sgbd+"://";
				break;
		}
		this.conexao = null;
		
	}
	
	public String getSgbd() {
		return sgbd;
	}
	public void setSgbd(String sgbd) {
		this.sgbd = sgbd;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Connection getConexao() {
		return conexao;
	}

	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}
	
	public void conectar()
	{
		try {
			this.conexao = DriverManager.getConnection(this.url,this.user,this.password);
			System.out.println("Conexão Estabelecida com Sucesso!");
		} catch (SQLException e) {
			System.err.println("Erro ao Conectar-se ao Banco de Dados!");
			e.printStackTrace();
		}
	}
	
	public void closeConection()
	{
		try {
			this.conexao.close();
			System.out.println("Conexão encerrada com o banco");
		} catch (SQLException e) {
			System.err.println("Erro ao Encerrar Conexão com o Banco de Dados!");
			e.printStackTrace();
		}
		
	}
        
    public boolean isConnected()
    {
        boolean result = false;
        try {
            if(this.conexao != null && !this.conexao.isClosed())
            {
                result = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
	public static void main(String[] args){
//		  ConnectionFactory con = new ConnectionFactory(SGBD.MySQL, "localhost/world", 3306, "root", "root");
		ConnectionFactory con = new ConnectionFactory(SGBD.PostgreSQL, "localhost", "exemplopoo", 5432, "welisson", "Jesus10");
		con.conectar();      
//		con.closeConection();
		Item item = new Item();
		item.setDescricao("refrigerande sabor guaraná");
		item.setMarca("Guaraná Antartica");
		item.setValor(5.5);
		String sql = "";
//		sql += "INSERT INTO public.itens VALUES(DEFAULT,?,?,?);";
		sql += "insert into \"public\".\"itens\" values(DEFAULT,?,?,?)";
		PreparedStatement stmt;
		try {
			stmt = con.conexao.prepareStatement(sql);
			stmt.setString(1, item.getMarca());
			stmt.setDouble(2, item.getValor());
			stmt.setString(3, item.getDescricao());
			stmt.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			con.closeConection();
		}
		//		EntityDao entityDao = new EntityDao("itens",con.getConexao());
//		entityDao.save(item);
	}

}
