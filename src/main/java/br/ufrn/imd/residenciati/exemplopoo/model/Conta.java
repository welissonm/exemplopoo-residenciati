/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.List;
import java.util.Objects;

import br.ufrn.imd.residenciati.exemplopoo.repositories.Entity;

/**
 *
 * @author itamir.filho
 */
public class Conta extends Entity{
    
    private List<ItemConta> itens;
    
    private Garcon garcon;
    
    private Cliente cliente;
    
    private Mesa mesa;

    public List<ItemConta> getItens() {
        return itens;
    }

    public void setItens(List<ItemConta> itens) {
        this.itens = itens;
    }

    public Garcon getGarcon() {
        return garcon;
    }

    public void setGarcon(Garcon garcon) {
        this.garcon = garcon;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Mesa getMesa() {
        return mesa;
    }

    public void setMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.itens);
        hash = 61 * hash + Objects.hashCode(this.garcon);
        hash = 61 * hash + Objects.hashCode(this.cliente);
        hash = 61 * hash + Objects.hashCode(this.mesa);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conta other = (Conta) obj;
        if (!Objects.equals(this.itens, other.itens)) {
            return false;
        }
        if (!Objects.equals(this.garcon, other.garcon)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.mesa, other.mesa)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Conta{" + "itens=" + itens + ", garcon=" + garcon + ", cliente=" + cliente + ", mesa=" + mesa + '}';
    }
    
    
   
}
