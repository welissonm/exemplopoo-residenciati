/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.Map;

import br.ufrn.imd.residenciati.exemplopoo.repositories.Entity;

/**
 *
 * @author itamir.filho
 */
public class Mesa extends Entity{
   
     
     private Integer numero;
     private Integer capacidade;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    public Integer getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(Integer capacidade) {
		this.capacidade = capacidade;
	}

	@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mesa other = (Mesa) obj;
        if (this.numero != other.numero) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mesa{" + "numero=" + numero + '}';
    }
    public Map<String, Class<?>> getColumns(){
    	HashMap<String, Class<?>> map = (HashMap<String, Class<?>>)super.getColumns();
    	map.put("numero", this.numero.getClass());
    	return map;
    }
    
}
