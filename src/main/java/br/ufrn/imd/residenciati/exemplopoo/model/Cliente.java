/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author itamir.filho
 */
public class Cliente extends Pessoa {
    
    private String cartaoFidelidade;
    
    private String credito;

	public String getCartaoFidelidade() {
        return cartaoFidelidade;
    }

    public void setCartaoFidelidade(String cartaoFidelidade) {
        this.cartaoFidelidade = cartaoFidelidade;
    }

    public String getCredito() {
        return credito;
    }
    
    public void setCredito(String credito) {
        this.credito = credito;
    }
    
     @Override
    public int hashCode() {
        int hash = 8;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Cliente other = (Cliente) obj;
        if (!((Pessoa)this).equals((Pessoa)obj)) {
            return false;
        }
        if(this.hashCode() != obj.hashCode()){
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public Map<String, Class<?>> getColumns(){
    	HashMap<String, Class<?>> map = (HashMap<String, Class<?>>)super.getColumns();
    	map.put("cartaoFidelidade", this.cartaoFidelidade.getClass());
    	map.put("credito",this.credito.getClass());
    	return map;
    }
    
    
}
