/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author itamir.filho
 */
public class Garcon extends Pessoa{
    
    private String matricula;
    
    private Float salario;
    
    private String carteiraTrabalho;

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public String getCarteiraTrabalho() {
        return carteiraTrabalho;
    }

    public void setCarteiraTrabalho(String carteiraTrabalho) {
        this.carteiraTrabalho = carteiraTrabalho;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.matricula);
        hash = 37 * hash + Objects.hashCode(this.carteiraTrabalho);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (!((Pessoa)this).equals((Pessoa)obj)) {
            return false;
        }
        final Garcon other = (Garcon) obj;
        if (!Objects.equals(this.matricula, other.matricula)) {
            return false;
        }
        if (!Objects.equals(this.carteiraTrabalho, other.carteiraTrabalho)) {
            return false;
        }
        return true;
    }
    @Override
    public Map<String, Class<?>> getColumns(){
//    	HashMap<String, Class<?>> map = (HashMap<String, Class<?>>)super.getColumns();
    	HashMap<String, Class<?>> map = new HashMap<String, Class<?>>();
    	map.put("nome", this.getNome().getClass());
    	map.put("Cpf",this.getCpf().getClass());
    	map.put("endereco",this.getEndereco().getClass());
    	map.put("telefone",this.getTelefone().getClass());
    	map.put("matricula", this.matricula.getClass());
    	map.put("salario",this.salario.getClass());
    	map.put("carteiraTrabalho",this.carteiraTrabalho.getClass());
    	return map;
    }
    
    
}
