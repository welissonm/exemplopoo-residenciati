/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.imd.residenciati.exemplopoo.repositories.Entity;

/**
 *
 * @author itamir.filho
 */
public class ItemConta extends Entity{
        
    private Conta conta;
    
    private Item item;
   
    private Integer quantidade;

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public double getValor() {
        return quantidade * item.getValor();
    }

       public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    public Map<String, Class<?>> getColumns(){
    	HashMap<String, Class<?>> map = (HashMap<String, Class<?>>)super.getColumns();
    	map.put("conta", this.conta.getClass());
    	map.put("item",this.item.getClass());
    	map.put("quantidade",this.quantidade.getClass());
    	return map;
    }
    
}
