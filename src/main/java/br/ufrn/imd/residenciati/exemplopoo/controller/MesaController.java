/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.controller;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Mesa;
import br.ufrn.imd.residenciati.exemplopoo.repositories.MesaRepository;

/**
 *
 * @author flawtista
 */
public class MesaController {
    private MesaRepository mesaRepository;
    
    public MesaController(MesaRepository mesaRepository){
        this.mesaRepository = mesaRepository;
    }
    
    public void salvar(Mesa mesa){
        this.mesaRepository.addMesa(mesa);
    }
    
    public void remove(Mesa mesa){
        this.mesaRepository.removeMesa(mesa);
    }
    
    public void atualizar(Mesa mesa){
        this.remove(mesa);
        this.salvar(mesa);
    }
    public List<String> listar(){
         List<String> lista = new ArrayList<String>();
        for (Mesa mesa : mesaRepository.listar()) {
            lista.add(mesa.toString());
        }
        return lista;
    }
}
