package br.ufrn.imd.residenciati.exemplopoo.repositories.framework;

public enum DriverJDBC {
	MySql("com.mysql.jdbc.Driver"),
	PostgreSql("org.postgresql.Driver");
	private String driver;
	DriverJDBC(String driver) {
		this.driver = driver;
	}
	public String getDriverName(){
		return this.driver;
	}
}
