/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Garcon;

/**
 *
 * @author flawtista
 */
public class GarconRepository {
    private final List<Garcon> garcons;
    
    public GarconRepository(){
        this.garcons = new ArrayList<Garcon>();
    }
    public void addGarcon(Garcon garcon){
        this.garcons.add(garcon);
    }
    public void removeGarcon(Garcon garcon){
        this.garcons.remove(garcon);
    }
    public List<Garcon> listar(){
        return garcons;
    }
}
