/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.controller;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.ItemConta;
import br.ufrn.imd.residenciati.exemplopoo.repositories.ItemContaRepository;

/**
 *
 * @author flawtista
 */
public class ItemContaController {
    private ItemContaRepository itemContaRepository;
    
    public ItemContaController(ItemContaRepository itemContaRepository){
        this.itemContaRepository = itemContaRepository;
    }
    
    public void salvar(ItemConta itemConta){
        this.itemContaRepository.addItemConta(itemConta);
    }
    
    public void remove(ItemConta itemConta){
        this.itemContaRepository.removeItemConta(itemConta);
    }
    
    public void atualizar(ItemConta itemContaOld, ItemConta itemContaNew){
        this.remove(itemContaOld);
        this.salvar(itemContaNew);
    }
    public List<String> listar(){
         List<String> lista = new ArrayList<String>();
        for (ItemConta itemConta : itemContaRepository.listar()) {
            lista.add(itemConta.toString());
        }
        return lista;
    }
}
