/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.Cliente;

/**
 *
 * @author flawtista
 */
public class ClienteRepository {
    private final List<Cliente> clientes;
    
    public ClienteRepository(){
        this.clientes = new ArrayList<Cliente>();
    }
    
    public void addCliente(Cliente cliente){
        this.clientes.add(cliente);
    }
    
    public void removeCliente(Cliente cliente){
        this.clientes.remove(cliente);
    }
    
    public List<Cliente> listar(){
        return this.clientes;
    }
    
    
}
