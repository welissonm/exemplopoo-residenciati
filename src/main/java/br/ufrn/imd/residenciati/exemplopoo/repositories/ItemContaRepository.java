/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.util.ArrayList;
import java.util.List;
import br.ufrn.imd.residenciati.exemplopoo.model.ItemConta;

/**
 *
 * @author flawtista
 */
public class ItemContaRepository {
    private final List<ItemConta> itemContas;
    
    public ItemContaRepository(){
        this.itemContas = new ArrayList<ItemConta>();
    }
    
    public void addItemConta(ItemConta itemConta){
        this.itemContas.add(itemConta);
    }
    
    public void removeItemConta(ItemConta itemConta){
        this.itemContas.remove(itemConta);
    }
    
    public List<ItemConta> listar(){
        return this.itemContas;
    }
}
