package br.ufrn.imd.residenciati.exemplopoo.repositories.framework;

public enum SGBD {
	MySQL, PostgreSQL, Firebird, SQLServer, IBMinformix, HSQLDB, DB2, mSQL, Oracle, TinySQL,
	ZODB, Jade, Sybase, MicrosoftAccess, MicrosoftVFoxpro;
}
