/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import br.ufrn.imd.residenciati.exemplopoo.repositories.Entity;


/**
 *
 * @author itamir.filho
 */
public class Pessoa extends Entity{

    
    private String nome;
    
    private String Cpf;
    private String endereco;
    private String telefone;

    public Pessoa(String nome, String cpf) {
        this.Cpf  = cpf;
        this.nome =  nome;
    }

    public Pessoa() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return Cpf;
    }

    public void setCpf(String Cpf) {
        this.Cpf = Cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pessoa other = (Pessoa) obj;
        if (!Objects.equals(this.Cpf, other.Cpf)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nome + " - " + this.Cpf;
    }
    @Override
    public Map<String, Class<?>> getColumns(){
    	HashMap<String, Class<?>> map = new HashMap<String, Class<?>>();
    	map.put("nome", this.nome.getClass());
    	map.put("Cpf",this.Cpf.getClass());
    	map.put("endereco",this.endereco.getClass());
    	map.put("telefone",this.telefone.getClass());
    	return map;
    }
    
  
    
}
