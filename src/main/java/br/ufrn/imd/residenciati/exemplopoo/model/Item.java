/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import br.ufrn.imd.residenciati.exemplopoo.repositories.Entity;


/**
 *
 * @author itamir.filho
 */
public class Item extends Entity{

    private String descricao;
    
    private Double valor;

    private String marca;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Item other = (Item) obj;
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao + " - " + marca + " - " + valor;
    }
    public Map<String, Class<?>> getColumns(){
    	HashMap<String, Class<?>> map =  new HashMap<String, Class<?>>();
    	map.put("descricao", this.descricao.getClass());
    	map.put("marca",this.marca.getClass());
    	map.put("valor",this.valor.getClass());
    	return map;
    }
    
    
}
