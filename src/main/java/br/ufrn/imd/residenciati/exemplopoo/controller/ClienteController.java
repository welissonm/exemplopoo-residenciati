/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.controller;

import br.ufrn.imd.residenciati.exemplopoo.model.Cliente;
import br.ufrn.imd.residenciati.exemplopoo.repositories.ClienteRepository;

/**
 *
 * @author flawtista
 */
public class ClienteController {
    
    private ClienteRepository clienteRepository;
    
    public ClienteController(ClienteRepository clienteRepository){
        this.clienteRepository = clienteRepository;
    }
    
    public void salvar(Cliente cliente){
        this.clienteRepository.addCliente(cliente);
    }
    
    public void remove(Cliente cliente){
        this.clienteRepository.removeCliente(cliente);
    }
    
    public void atualizar(Cliente clienteOld, Cliente clienteNew){
        this.remove(clienteOld);
        this.salvar(clienteNew);
    }
    public String listar() {
        String lista = "Lista de clientes \n";
        for (Cliente cliente : clienteRepository.listar()) {
            lista += cliente + "\n";
        }
        return lista;
    }
}
