package br.ufrn.imd.residenciati.exemplopoo;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.ufrn.imd.residenciati.exemplopoo.model.Cliente;
import br.ufrn.imd.residenciati.exemplopoo.model.Item;
import br.ufrn.imd.residenciati.exemplopoo.view.MainView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author flawtista
 */
public class Aplication {
    private final String title;
    private MainView window;
    
    private EntityManagerFactory factory;
    private EntityManager manager;
    
    public Aplication(){
        this.title = "Bar Admin";
        this.initComponents();
        Cliente client = new Cliente();
        client.setNome("Welisson Moura");
        client.setCpf("07116021400");
        client.setEndereco("Rua projetada sn");
        client.setTelefone("8499999-9999");
        Item item = new Item();
        item.setDescricao("Café");
        item.setMarca("Pilão");
        item.setValor(4.5);
//        this.factory = Persistence.createEntityManagerFactory("exemplopoo");
//        this.manager = factory.createEntityManager();
//        this.manager.getTransaction().begin();
//        manager.persist(client);
//        manager.persist(item);
//        manager.getTransaction().commit();
//        System.out.println(client.getId());
//        System.out.println(item.getId());
//        manager.close();
//        this.window.getContentPane().add(window);
    }
    
    private void initComponents(){
//        this.window = new JFrame(this.title);
        this.window = new MainView();
    }
    public void setVisible(boolean visible){
        this.window.setVisible(visible);
    }
    public boolean isVisible(){
        return this.window.isVisible();
    }
    public static void main(String[] args){
        Aplication app = new Aplication();
        app.setVisible(true);
    }
}
