/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories.implments;

import java.util.Map;

import br.ufrn.imd.residenciati.exemplopoo.repositories.interfaces.IAbstractEntity;

/**
 *
 * @author Welisson M Santos
 */
public abstract class AbstractEntity implements IAbstractEntity{
    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    
    abstract public Map<String, Class<?>> getColumns();
    
}
