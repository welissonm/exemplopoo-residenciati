/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories.framework;

import br.ufrn.imd.residenciati.exemplopoo.repositories.implments.AbstractEntity;
import br.ufrn.imd.residenciati.exemplopoo.repositories.interfaces.IDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Welisson M Santos
 * @param <T>
 */
public abstract class AbstractEntityDao<T extends AbstractEntity> implements IDAO<T> {

    protected Connection conexao;
    private final String tabela;
    protected boolean connectionInherited;

    public AbstractEntityDao(String tabela, Connection connection) {
        this.tabela = tabela;
        this.conexao = connection;
        try {
            if (connection != null && !connection.isClosed()) {
                this.connectionInherited = true;
            } else {
                this.connectionInherited = false;
            }
        } catch (SQLException ex) {
            this.connectionInherited = false;
            Logger.getLogger(AbstractEntityDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public abstract void save(T entity) throws Exception;

    @Override
    public abstract void update(T entity) throws Exception;

    @Override
    public void delete(T entity) throws Exception {
        try {
            this.initConexao();
            PreparedStatement stmt = this.conexao.prepareStatement("DELETE"
                    + " FROM " + this.tabela + " WHERE id = ?");
            stmt.setLong(1, entity.getId());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (!this.connectionInherited) {
                this.conexao.close();
            }
        }
    }

    @Override
    public abstract T find(Long id) throws Exception;

    @Override
    public abstract List<T> find(Long start, Long last) throws Exception;

    @Override
    public abstract List<T> findAll() throws Exception;

    @Override
    public int count() throws Exception {
        String sql = "SELECT COUNT(*) FROM " + this.tabela;
        int contagem = -1;
        try {
            this.initConexao();
            PreparedStatement stmt = this.conexao.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            contagem = rs.getInt(1);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        } finally {
            if (!this.connectionInherited) {
                this.conexao.close();
            }
        }
        return contagem;
    }

    protected String getTabela() {
        return this.tabela;
    }

    protected Connection getConexao() {
        return this.conexao;
    }

    protected void initConexao() throws SQLException {
        if (this.connectionInherited && this.conexao == null
                || this.connectionInherited && this.conexao.isClosed()) {
            this.conexao = SessionBD.getInstance().getConexao();
            this.connectionInherited = false;
        } else if (this.conexao == null || this.conexao.isClosed()) {
            this.conexao = SessionBD.getInstance().getConexao();
        }
    }

}
