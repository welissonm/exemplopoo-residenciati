/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufrn.imd.residenciati.exemplopoo.repositories.interfaces;

import java.io.Serializable;


import java.util.List;

/**
 * Interface padrão para o CRUD em BD relacional e a base para as demais classes
 * que controlam o fluxo das operações de cada entidade.
 * @author welisson
 * @param <T>
 */
public interface IDAO <T> extends Serializable{
    
    public void save(T entity) throws Exception;
    public void update(T entity) throws Exception;
    public void delete(T entity) throws Exception;
    public T find(Long id) throws Exception;
    public List<T> find(Long start, Long last) throws Exception;
    public List<T> findAll() throws Exception;
    public int count() throws Exception;
}
