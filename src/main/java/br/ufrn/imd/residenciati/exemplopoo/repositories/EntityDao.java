package br.ufrn.imd.residenciati.exemplopoo.repositories;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import br.ufrn.imd.residenciati.exemplopoo.repositories.framework.AbstractEntityDao;

public class EntityDao extends AbstractEntityDao<Entity> {

	public EntityDao(String tabela, Connection connection) {
		super(tabela, connection);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1582553828877421384L;

	@Override
	public void save(Entity entity) throws Exception {
		String sql = "";
		String str =""
;		int count = 0;
		HashMap<String, Class<?>> map = (HashMap<String, Class<?>>)entity.getColumns();
		sql += "INSERT INTO public."+this.getTabela()+ " VALUES (null,";
		for(int i = 1;i<map.size();i++) {
			sql+="?,";
		}
		sql+="?)";
		try {
			this.initConexao();
			PreparedStatement stmt = this.getConexao().prepareStatement(sql);
			Set<String> chaves = map.keySet();
			for(String chave : chaves) {
				if(chave != null) {
					count++;
					str = "get"+chave.replace(chave.substring(0, 1), chave.substring(0, 1).toUpperCase());
					Method method = entity.getClass().getMethod(str);
					if(map.get(chave).equals(String.class)) {
						stmt.setString(count, (String)method.invoke(entity));
					}else if(map.get(chave).equals(Integer.class)) {
						stmt.setInt(count, (Integer)method.invoke(entity));
					}else if(map.get(chave).equals(Float.class)) {
						stmt.setFloat(count,(Float)method.invoke(entity));
					}else if(map.get(chave).equals(Double.class)) {
						stmt.setDouble(count,(Double)method.invoke(entity));
					}else {
						System.err.println("Tipo a ser persistindo desconhecido");
					}
				}
				
			}
			stmt.execute();
		}catch(SQLException ex){
			throw new RuntimeException(ex);
		}finally
		{
			this.getConexao().close();
		}

	}

	@Override
	public void update(Entity entity) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Entity find(Long id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Entity> find(Long start, Long last) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Entity> findAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
