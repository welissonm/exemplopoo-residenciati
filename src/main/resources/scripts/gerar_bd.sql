--Criando o banco de dados
createdb exemplopoo;
--Creando as tabelas das entidades com o schema public
CREATE SEQUENCE public.mesas_id_seq MINVALUE 1 MAXVALUE 9223372036854775807 START WITH 1 INCREMENT BY 1 CACHE 1;
CREATE TABLE public.mesas (
	id bigint NOT NULL DEFAULT nextval('mesas_id_seq'::regclass),
	numero integer NOT NULL,
	capacidade integer NOT NULL,
    "CREATED_AT" timestamp(6) without time zone,
    "DELETED_AT" timestamp(6) without time zone,
    "UPDATED_AT" timestamp(6) without time zone,
    CONSTRAINT mesas_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;